import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './appRouting.module';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // formularios e inputs
import { HttpClientModule } from '@angular/common/http'; // para trabajar con token http

// componentes
import { AppComponent } from './app.component';
import { PathComponent } from './modules/path/path.component';
// ---- Direcciones
import { DireccionesComponent } from './modules/direcciones/direcciones.component';
import { DireccionNuevaComponent } from './modules/direcciones/direccion-nueva/direccion-nueva.component';
import { DireccionDetalleComponent } from './modules/direcciones/direccion-detalle/direccion-detalle.component';
// ---- Formas de Pago
import { FormasDePagoComponent } from './modules/formas-de-pago/formas-de-pago.component';
import { TarjetaCreditoComponent } from './modules/formas-de-pago/tarjeta-credito/tarjeta-credito.component';
import { PaypalComponent } from './modules/formas-de-pago/paypal/paypal.component';
import { TiendasComponent } from './modules/formas-de-pago/tiendas/tiendas.component';
import { CertificadoMonederoComponent } from './modules/formas-de-pago/certificado-monedero/certificado-monedero.component';
import { PuntosComponent } from './modules/formas-de-pago/puntos/puntos.component';
import { DepositoTransferenciaComponent } from './modules/formas-de-pago/deposito-transferencia/deposito-transferencia.component';
import { UsuarioComponent } from './modules/confirmacion/usuario/usuario.component';
import { ConfirmacionPaypalComponent } from './modules/confirmacion/confirmacion-paypal/confirmacion-paypal.component';
import { ConfirmacionDepositoTransferenciaComponent } from './modules/confirmacion/confirmacion-deposito-transferencia/confirmacion-deposito-transferencia.component';
import { ConfirmacionTiendaComponent } from './modules/confirmacion/confirmacion-tienda/confirmacion-tienda.component';
import { MensualidadesComponent } from './modules/formas-de-pago/tarjeta-credito/mensualidades.component/mensualidades.component';
import { PromocionesComponent } from './modules/formas-de-pago/tarjeta-credito/promociones/promociones.component';

// ---- Resumen de Compra
import { ResumenCompraComponent } from './modules/resumen-compra/resumen-compra.component';
// ---- Confirmacion
import { ConfirmacionComponent } from './modules/confirmacion/confirmacion.component';
// ---- ThankyouPage
import { ThankyouPageComponent } from './modules/thankyou-page/thankyou-page.component';
// ---- spinner
import { SpinnerComponent } from './modules/spinner/spinner.component';

// extensiones
import { CardModule } from 'ngx-card/ngx-card'; // para tarjetas
import { CookieService } from 'ngx-cookie-service';

// servicios
import { ConfigService } from './core/services/config.service';
import { ValidatorService } from './core/services/validator.service';
import { GlobalService } from './core/services/global.service';
import { ZonificacionService } from './core/services/zonificacion.service';
import { FormasPagoService } from './core/services/formasPago.service';
import { ErrorService } from './core/services/error.service';
import { CardValidatorService } from './core/services/card-validator.service';
import { DepositoService } from './core/services/deposito.service';
import { MonederoService } from './core/services/monedero.service';
import { DireccionesService } from './core/services/direcciones.service';
import { DetalleProductoComponent } from './modules/resumen-compra/detalle-producto/detalle-producto.component';

@NgModule({
  declarations: [
    AppComponent,
    DireccionNuevaComponent,
    DireccionesComponent,
    DireccionDetalleComponent,
    PathComponent,
    FormasDePagoComponent,
    TarjetaCreditoComponent,
    PaypalComponent,
    TiendasComponent,
    CertificadoMonederoComponent,
    PuntosComponent,
    DepositoTransferenciaComponent,
    ResumenCompraComponent,
    DetalleProductoComponent,
    ThankyouPageComponent,
    SpinnerComponent,
    ConfirmacionComponent,
    UsuarioComponent,
    ConfirmacionPaypalComponent,
    ConfirmacionDepositoTransferenciaComponent,
    ConfirmacionTiendaComponent,
    MensualidadesComponent,
    PromocionesComponent
  ],
  imports: [
    BrowserModule,
    // FormBuilder,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CardModule,
    AppRoutingModule,
  ],
  providers: [
    CookieService,
    ValidatorService,
    GlobalService,
    ZonificacionService,
    FormasPagoService,
    ErrorService,
    CardValidatorService,
    DepositoService,
    MonederoService,
    ConfigService,
    DireccionesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
