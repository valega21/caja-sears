import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-path',
  templateUrl: './path.component.html',
  styleUrls: ['./path.component.sass']
})
export class PathComponent implements OnInit {
    /**
     * bandera mostrar breadcrumb
     */
    public breadcrumb = false;
    /**
     * campo ruta
     */
    @Input() ruta;

    /**
     * constructor
     */
  constructor() { }

  ngOnInit() {
  }

}
