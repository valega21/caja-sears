import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-usuario-component',
    templateUrl: './usuario.component.html',
    styleUrls: ['./usuario.component.sass']
})
export class UsuarioComponent implements OnInit {

    // tslint:disable-next-line:no-input-rename
    @Input('nombre') nombre: string;

    constructor() {

    }

    ngOnInit() {
    }
}
