import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { isNumber } from 'util';
import { Subscription } from 'rxjs/Subscription';
import { Direccion } from '../../core/modelos/direccion';
import { Tarjeta } from '../../core/modelos/tarjeta';
import { Checkout } from '../../core/modelos/checkout';
import { Validaciones } from '../../core/modelos/validaciones';

const fpTarjtasSears = 1;
const fpTarjtasDC = 2;
const fpPaypal = 3;
const fpTienda = 4;
const fpMonedero = 5;
const fpPuntos = 6;
const fpDeposito = 7;


@Component({
  selector: 'app-confirmacion',
  templateUrl: './confirmacion.component.html',
  styleUrls: ['./confirmacion.component.sass']
})
export class ConfirmacionComponent implements OnInit {

  @Input() checkout: Checkout = new Checkout();
  /**
   * titulo principal de la sección
   */
  public title = 'Confirmación';
  /**
   * nombre de la ruta actual activa
   */
  public ruta = 'confirmacionActiva';
  /**
  * datos direccion
  */
  public direccion: Direccion = new Direccion();
  public tarjeta: Tarjeta = new Tarjeta();
  public nombre: string;
  public formaPago: number; // number;
  public id_forma: number;
  public showEdit = true;
  public paymentTitle: string;
  public facturacion: Direccion = new Direccion();

  /**
  * bandera para desactivar los botones
  */
  public disabled = true;
  /**
  * subcripcion al servicio global
  */
  public codeSubscription: Subscription;
  /**
   * bandera cargo contenido
   */
  public isLoad = true; // false

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.arriba();
  }

  /**
   * realiza el proceso de pago
   */
  pagar(): void {
    console.log('Presiono Pagar...');
    switch (this.formaPago) {
      case fpPaypal:
        this.processCheckout();
        break;
      case fpDeposito:
        this.processCheckout();
        break;
    }
  }

  /**
  * posiciona en la parte superior de la pantalla
  */
  public arriba(): void {
    window.scrollTo(0, 0);
  }

  /**
   * revisa el proceso de pago utilizado
  */
  public processCheckout(): void {
    this.goThanckYouPage();
  }

  /**
  * Redirecciona a la página thanck you page
  */
  goThanckYouPage(): void {
    this.router.navigate(['/gracias-por-tu-compra']);
  }

}
