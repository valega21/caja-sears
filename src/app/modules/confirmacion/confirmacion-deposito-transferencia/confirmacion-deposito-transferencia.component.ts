import { Component, Input, OnInit } from '@angular/core';
import { GlobalService } from '../../../core/services/global.service';
import { DepositoService } from '../../../core/services/deposito.service';

@Component({
    selector: 'app-confirmacion-deposito-transferencia',
    templateUrl: './confirmacion-deposito-transferencia.component.html',
    styleUrls: ['./confirmacion-deposito-transferencia.component.sass']
})

export class ConfirmacionDepositoTransferenciaComponent implements OnInit {

    public loading = false;
    public nombreBanco: string;
    // tslint:disable-next-line:no-input-rename
    @Input('showEdit') showEdit = false;

    constructor(
        private globalService: GlobalService,
        private depositoService: DepositoService
    ) { }

    ngOnInit() {
        this.getBanco().then((banco) => {
            this.nombreBanco = 'bancomer'; // banco.banco

        });
    }

    /**
     * obtiene el banco
     */
    public getBanco() {
        return this.depositoService.getBanco().toPromise();
    }
}
