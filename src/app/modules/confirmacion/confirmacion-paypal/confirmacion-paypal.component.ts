import { Component, Input } from '@angular/core';
import { Checkout } from '../../../core/modelos/checkout';

@Component({
    selector: 'app-confirmacion-paypal',
    templateUrl: './confirmacion-paypal.component.html',
    styleUrls: ['./confirmacion-paypal.component.sass']
})
export class ConfirmacionPaypalComponent {

    @Input() checkout: Checkout = new Checkout();
    // tslint:disable-next-line:no-input-rename
    @Input('showEdit') showEdit = false;

    constructor() { }

}
