import { Component, OnInit, Input } from '@angular/core';
import { Multipedido } from '../../../core/modelos/multipedido';

@Component({
    selector: 'app-confirmacion-tienda',
    templateUrl: './confirmacion-tienda.component.html',
    styleUrls: ['./confirmacion-tienda.component.sass']
})
export class ConfirmacionTiendaComponent implements OnInit {

    // tslint:disable-next-line:no-input-rename
    @Input('showEdit') showEdit = false;
    // tslint:disable-next-line:no-input-rename
    @Input('multipedido') multipedido: Multipedido = new Multipedido();
    public tienda = 'SEARS';

    constructor() {

    }

    ngOnInit() {
    }
}
