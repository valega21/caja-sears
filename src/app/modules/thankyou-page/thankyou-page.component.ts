import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-thankyou-page',
  templateUrl: './thankyou-page.component.html',
  styleUrls: ['./thankyou-page.component.sass']
})
export class ThankyouPageComponent implements OnInit {

  public isLoad = true; // false;
  public hidden = false; // true;

  constructor() { }

  ngOnInit() {
  }

}
