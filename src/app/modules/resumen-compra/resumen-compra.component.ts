import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResumenCompra } from '../../core/modelos/resumenCompra';
import { Cupon } from '../../core/modelos/cupon';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-resumen-compra',
  templateUrl: './resumen-compra.component.html',
  styleUrls: ['./resumen-compra.component.sass']
})
export class ResumenCompraComponent implements OnInit {

  @Input() id_forma = '';
  @Input() bim = '';
  public detalleCompra: ResumenCompra;
  public loading = false;
  public infoCargada = true; // false;
  public banderaCupon = false;
  public cupon = new Cupon();

  checkoutForm;

  constructor(public client: HttpClient, private formBuilder: FormBuilder) {
    this.checkoutForm = this.formBuilder.group({
      cupon: ''
    });
  }

  ngOnInit() {
  }

  ver_cupon() {
    this.banderaCupon = !this.banderaCupon;
  }

  onSubmit(customerData) {
    // Process checkout data here
    if (!customerData.cupon) {
      window.confirm('El campo cupón no es válido');
    } else {
      window.confirm('El campo cupón es: ' + customerData.cupon);
      this.checkoutForm.reset();
    }
  }
}
