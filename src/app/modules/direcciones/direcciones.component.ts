import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Direccion } from '../../core/modelos/direccion';
import { DireccionesService } from '../../core/services/direcciones.service';

@Component({
  selector: 'app-direcciones',
  templateUrl: './direcciones.component.html',
  styleUrls: ['./direcciones.component.sass']
})
export class DireccionesComponent implements OnInit {

  public ruta = 'direccionActiva';
  public title = 'Elige tu Dirección de Envío';
  public title3 = 'Resumen de Compra';
  public url: '/';
  public direcciones = [];
  public banderaPredeterminada = false;
  public identificadorDireccion = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private servicioDirecciones: DireccionesService) {
    this.url = '/';
  }

  ngOnInit() {
    this.getDirecciones();
  }

  /**
   * obtener listado de direcciones
   */
  getDirecciones() {
    this.servicioDirecciones.getDirecciones()
      .subscribe(direcciones => {
        this.direcciones = direcciones;
        console.log(this.direcciones);
        this.establecerDireccionPredeterminada();
      });
  }

  /**
   * enviar a la dirección predeterminada
   */
  continuar(): void {
    this.router.navigate(['/formas-pago']);
  }

  /**
   * establecer dirección como predeterminada
   * @param identificador, identificador de la dirección seleccionada
   */
  direccionPorDefecto(identificador: number): void {
    this.servicioDirecciones.defaultDireccion(identificador)
      .subscribe(() => {
        this.getDirecciones();
      });
  }

  /**
   * establer dirección como predeterminada
   */
  establecerDireccionPredeterminada(): void {
    for (const direccion of this.direcciones) {
      if (+direccion.accesibilidad === 1) {
        this.identificadorDireccion = direccion.id;
      }// end if
    }// end for

    if (this.identificadorDireccion === 0) {
      if (this.direcciones.hasOwnProperty(0)) {
        this.identificadorDireccion = this.direcciones[0].id;
        this.direcciones[0].accesibilidad = '1';
      }
    }// end if
  }

  /**
   * eliminar dirección seleccionada
   * @param id , identificador de la direccion
   */
  eliminar(id: number): void {
    this.servicioDirecciones.deleteDireccion(id)
      .subscribe(() => {
        this.getDirecciones();
      });
  }

}
