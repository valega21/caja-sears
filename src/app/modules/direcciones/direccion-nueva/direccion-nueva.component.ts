import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';

import { Validaciones } from '../../../core/modelos/validaciones';
import { Direccion } from '../../../core/modelos/direccion';
import { Zonificacion } from '../../../core/modelos/zonificacion';
import { ValidatorService } from '../../../core/services/validator.service';
import { ZonificacionService } from '../../../core/services/zonificacion.service';

@Component({
  selector: 'app-direccion-nueva',
  templateUrl: './direccion-nueva.component.html',
  styleUrls: ['./direccion-nueva.component.sass']
})
export class DireccionNuevaComponent implements OnInit {

  @Input() direccion: Direccion;
  @Input() colonias: String[] = [];
  public zonas: Zonificacion[];
  public title = 'Agregar Nueva Dirección';
  public title1 = 'Datos de Contacto';
  public title2 = 'Dirección de Entrega';
  public title3 = 'Detalle de tu Compra';
  public error = false;
  public mensaje = 'En nuestros registros no encontramos tu C.P, intenta con otro';
  public default = true;
  public hide = true;
  public presiono = false;
  public ruta = 'direccionActiva';
  public url = '/direcciones/agregar';
  public mostrarCampos = false;

  public rules = {
    'nombre': new Validaciones(
      'nombre',
      '',
      'string',
      'El campo nombre es requerido.'
    ),
    'apellidoP': new Validaciones(
      'apellidoP',
      '',
      'string',
      'El campo Apellido Paterno es requerido.'
    ),
    'apellidoM': new Validaciones(
      'apellidoM',
      '',
      'string',
      'El campo Apellido Materno es requerido.'
    ),
    'telefono': new Validaciones(
      'telefono',
      '',
      'phone',
      'El campo teléfono es requerido.'
    ),
    'personaAutorizada': new Validaciones(
      'personaAutorizada',
      '',
      'string',
      'El campo Persona Autorizada es requerido.'
    ),
    'calle': new Validaciones(
      'calle',
      '',
      'string',
      'El campo calle es requerido.'
    ),
    'nExt': new Validaciones(
      'nExt',
      '',
      'string',
      'El campo número exterior es requerido.'
    ),
    'colonia': new Validaciones(
      'colonia',
      '',
      'string',
      'El campo colonia es requerido.'
    ),
    'municipio': new Validaciones(
      'municipio',
      '',
      'string',
      'El campo municipio es requerido.'
    ),
    'ciudad': new Validaciones(
      'ciudad',
      '',
      'string',
      'El campo ciudad es requerido.'
    ),
    'estado': new Validaciones(
      'estado',
      '',
      'string',
      'El campo estado es requerido.'
    ),
    'entrega': new Validaciones(
      'entrega',
      '',
      'string',
      'El campo entrega es obligatorio.'
    ),
    'nivel': new Validaciones(
      'nivel',
      '',
      'string',
      'El campo nivel es obligatorio.'
    ),
    'acceso': new Validaciones(
      'acceso',
      '',
      'string',
      'El campo acceso es obligatorio.'
    ),
    'codigoPostal': new Validaciones(
      'codigoPostal',
      '',
      'postalCode',
      'El campo código postal es obligatorio'
    ),
  };

  constructor(
    private validatorService: ValidatorService,
    private zonificacionService: ZonificacionService,
    private router: Router,
    private route: ActivatedRoute, ) {
    this.direccion = {
      id: 0, nombre: '', apellidoP: '',
      apellidoM: '', telefono: '', personaAutorizada: '',
      nExt: '', entrega: '', nivel: '', municipio: '', calle: '',
      estado: '', ciudad: '', entreCalles: '', colonia: '',
      referencias: '', numeroInterior: '', codigoPostal: '', acceso: ''
    };
  }

  ngOnInit() {
    this.zonificacionService.getZonificacion();
  }


  /**
   * Guardar el objeto dirección
   */
  save(): void {

    const validate = this.validateForm(undefined);
    this.presiono = true;
    if (validate) {
      console.log('se envio');
    } else {
      console.log('no se envio');
    }// end if
  }

  /**
   * Genera la validación del formulario
   * @param {string | undefined} field
   * @returns {boolean}
   */
  public validateForm(field: string | undefined): boolean {
    let valid = true;
    let response = this.rules;
    this.setRulesValidate(field);
    if (field === undefined) {
      response = this.validatorService.validate(this.rules);
    } else {
      const param = { [field]: this.rules[field] };
      response = this.validatorService.validate(param);
    }
    for (const key in response) {
      if (response.hasOwnProperty(key)) {
        if (response[key].error) {
          this.rules[key].error = response[key].error;
          valid = false;
        }// end if
      }// end if
    }// end for
    return valid;
  }

  /**
   * Define las reglas de validación para el formulario
   * @param {string | undefined} field
   */
  private setRulesValidate(field: string | undefined): void {
    if (field !== undefined) {
      this.rules[field].value = this.direccion[field];
      console.log('no esta vacio el nombre');
    } else {
      console.log('si esta vacio el nombre');
      this.rules.nombre.value = this.direccion.nombre;
      this.rules.apellidoP.value = this.direccion.apellidoP;
      this.rules.apellidoM.value = this.direccion.apellidoM;
      this.rules.telefono.value = this.direccion.telefono;
      this.rules.personaAutorizada.value = this.direccion.personaAutorizada;
      this.rules.entrega.value = this.direccion.entrega;
      this.rules.nivel.value = this.direccion.nivel;
      this.rules.acceso.value = this.direccion.acceso;
      this.rules.municipio.value = this.direccion.municipio;
      this.rules.calle.value = this.direccion.calle;
      this.rules.ciudad.value = this.direccion.ciudad;
      this.rules.estado.value = this.direccion.estado;
      this.rules.nExt.value = this.direccion.nExt;
      this.rules.codigoPostal.value = this.direccion.codigoPostal;
      this.rules.colonia.value = this.direccion.colonia;
    }// end if
  }

  /**
  * Obtener zonificación
  * @param {string} cp
  */
  getZoning(cp: string): void {
    if (cp !== undefined && cp.length >= 5) {
      this.zonificacionService.getZonificacion().subscribe(data => {
        this.updateZoning(data);
        this.mostrarCampos = !this.mostrarCampos;
      });

    } else {
      this.rules.codigoPostal.error = true;
    }// end if
  }

  /**
  * Actualiza los campos de zonificación
  * @param {Zonificacion} zonificacion
  */
  updateZoning(zonificacion: Zonificacion): void {
    this.direccion.estado = zonificacion.estado;
    this.direccion.ciudad = (zonificacion.ciudad && zonificacion.ciudad.length > 1) ? zonificacion.ciudad : 'N/A';
    this.direccion.municipio = zonificacion.municipio;
    this.direccion.colonia = undefined;
    this.default = true;
    this.colonias = zonificacion.colonias;
    if (!zonificacion.estatus) {
      this.error = true;
      this.hide = true;
      this.mensaje = 'En nuestros registros no encontramos tu C.P, intenta con otro'; // zonificacion.error;
    } else {
      this.error = false;
      this.mensaje = '';
      this.rules.codigoPostal.error = false;
    }
  }

  public routerBack() {
    // if (this.url === '/direcciones') {
    if (this.url === '/direcciones/agregar') {
      this.router.navigate(['/direcciones']);
    } else {
      window.location.href = 'direcciones';
    }// end if
  }
}
