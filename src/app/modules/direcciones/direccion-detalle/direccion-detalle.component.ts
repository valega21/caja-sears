import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-direccion-detalle',
  templateUrl: './direccion-detalle.component.html',
  styleUrls: ['./direccion-detalle.component.sass']
})
export class DireccionDetalleComponent implements OnInit {

  public title = 'Editar Dirección';
  public ruta = 'direccionActiva';
  public url = '/direcciones';
  public title3 = 'Resumen de Compra';
  public saveClicked = false;
  formularioDireccion: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.validarFormulario();
  }

  validarFormulario() {
    this.formularioDireccion = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellidoP: ['', Validators.required],
      apellidoM: ['', Validators.required],
      telefono: ['', Validators.required],
      calle: ['', Validators.required],
      numExterior: ['', Validators.required],
      numInterior: ['', Validators.required],
      cp: ['', Validators.required],
      colonia: ['', Validators.required],
      municipio: ['', Validators.required],
      estado: ['', Validators.required],
      ciudad: ['', Validators.required],
      personaAutorizada: ['', Validators.required],
      edificacion: ['', Validators.required],
      nivel: ['', Validators.required],
      forma: ['', Validators.required]
    });
  }

  save(): void {
    this.saveClicked = true;

    if (this.formularioDireccion.valid) {
      alert('formulario valido');
    }
  }

}
