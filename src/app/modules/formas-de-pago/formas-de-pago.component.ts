import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { FormaPago } from '../../core/modelos/formasPago';
import { FormasPagoService } from '../../core/services/formasPago.service';

@Component({
  selector: 'app-formas-de-pago',
  templateUrl: './formas-de-pago.component.html',
  styleUrls: ['./formas-de-pago.component.sass']
})
export class FormasDePagoComponent implements OnInit, OnDestroy {

  public title = 'Selecciona una forma de Pago';
  public pendientePago2 = 'Elige otra forma de pago';
  public ruta = 'pagosActiva';
  public hide = true;
  public url = '/direcciones';
  public assets = {};
  public order = {};
  public formasDePago: FormaPago[];

  constructor(private route: ActivatedRoute, private router: Router, private formasPagoService: FormasPagoService) {
    this.assets[1] = { url: '/formas-pago/tarjeta-sears', img: './assets/logo-sears1.svg', name: 'Tarjeta departamental Sears' };
    this.assets[2] = { url: '/formas-pago/tarjetas-credito', img: './assets/logo-visa.svg', name: 'Tarjeta Visa / Mastercard / AMEX' };
    this.assets[3] = { url: '/confirmacion', img: './assets/logo-paypal.svg', name: 'Paypal' };
    this.assets[4] = { url: '/confirmacion', img: '/assets/logo-sears1.svg', name: 'Pago en Tienda Sears' };
    this.assets[5] = { url: '/formas-pago/certificado-monedero', img: './assets/logo-monedero-sears.jpg', name: 'Certificado / Monedero Sears' };
    this.assets[6] = { url: '/formas-pago/puntos-sears', img: './assets/logo-puntos-sears.webp', name: 'Puntos Sears' };
    this.assets[7] = { url: '/formas-pago/deposito-transferencia', img: './assets/logo-bancos.svg', name: 'Depósito y Transferencia Bancaria' };

    this.order[1] = { weight: 0 };
    this.order[2] = { weight: 1 };
    this.order[3] = { weight: 2 };
    this.order[4] = { weight: 3 };
    this.order[5] = { weight: 4 };
    this.order[6] = { weight: 5 };
    this.order[7] = { weight: 6 };

  }

  ngOnInit() {
    this.getFormasDePago();
  }

  ngOnDestroy() {

  }

  public getFormasDePago(): void {

    this.formasDePago = this.formasPagoService.getFormasDePago();
    // .subscribe(formasDePago => {
    //   this.formasDePago = formasDePago;
    //   console.log(this.formasDePago);
    //   this.orderFormasDePago();

    // });
  }

  /**
   * Establece la forma de pago seleccionada
   * @param {number} id
   */
  public setFormaDePago(id: number): void {

    // this.formasPagoService.setFormaDePago(id)
    //   .subscribe(() => {
    //     // DataLayer Forma Pago
    //     this.datalayerService.setCheckoutStep3(id);
        this.routeFormaPago(id);

      // });
  }

  /**
   * redirecciona a la forma de pago
   * @param {number} id
   */
  public routeFormaPago(id: number): void {

    if (this.assets.hasOwnProperty(id)) {
      this.router.navigate([this.assets[id].url]);
    }// end if
  }
}
