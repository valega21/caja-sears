import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-tarjeta-departamental',
  templateUrl: './tarjeta-departamental.component.html',
  styleUrls: ['./tarjeta-departamental.component.sass']
})
export class TarjetaDepartamentalComponent implements OnInit, OnDestroy {
  public title = 'Agregar Nueva Forma de Pago';
  public ruta = 'pagosActiva';
  public cvvHelp = 'El cvv se encuentra en la parte posterior de la tarjeta';
  public breadcrumb = true;
  public id = 0;

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() { }

}
