import { Component, OnInit } from '@angular/core';
// import { Mensualidad } from '../../../entities/mensualidad';
// import { Promocion } from '../../../entities/promocion';
// import { PromocionesService } from '../../../services/promociones.service';
import { Router } from '@angular/router';
// import { DatalayerService } from '../../../services/datalayer.service';

@Component({
  selector: 'app-promociones',
  templateUrl: './promociones.component.html',
  styleUrls: ['./promociones.component.sass']
})

export class PromocionesComponent implements OnInit {

  public title = 'Elige una promoción';
  public subtitle = 'Promociones con tarjeta Sears';
  public loading = true;
  public titlePromocion: string;
  public nombreFormaPago: string;
  public ruta = 'pagosActiva';
  public response: any;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

  /**
   * guardar datos de la promoción
   */
  save(): void {
    this.response = true;
    this.goConfirmacion(this.response);
  }

  /**
  * redirige a la pagina de confirmación
  * @param {any} response
  */
  private goConfirmacion(response: any): void {
    if (response === true) {
      this.router.navigate(['/confirmacion']);
    }// end if

  }
}
