import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { isNumber, isString } from 'util';
// servicios
import { CardValidatorService } from '../../../core/services/card-validator.service';
import { ErrorService } from '../../../core/services/error.service';
import { GlobalService } from '../../../core/services/global.service';
// modelos
import { TarjetaForm } from '../../../core/modelos/tarjetaForm';
import { Expiry } from '../../../core/modelos/expiry';
import { Validaciones } from '../../../core/modelos/validaciones';
import { Zonificacion } from '../../../core/modelos/zonificacion';
import { Direccion } from '../../../core/modelos/direccion';
import { Tarjeta } from '../../../core/modelos/tarjeta';

@Component({
  selector: 'app-tarjeta-credito',
  templateUrl: './tarjeta-credito.component.html',
  styleUrls: ['./tarjeta-credito.component.sass']
})
export class TarjetaCreditoComponent implements OnInit, OnDestroy {

  @Input() expiry: Expiry = new Expiry();
  public tarjetaForm: TarjetaForm = new TarjetaForm();
  @Input() direccionForm: Direccion = new Direccion();
  public direccion: Direccion = new Direccion();

  /**
   * titulo principal de la sección
   */
  public title = 'Agregar Nueva Forma de Pago';
  /**
   * ruta activa para la seccion
   */
  public ruta = 'pagosActiva';
  /**
   * mensaje de ayuda para visualizar el cvv
   */
  public cvvHelp = 'El cvv se encuentra en la parte posterior de la tarjeta';
  public breadcrumb = true;
  public id = 0;
  /** ocultar radio Button */
  public hide = false;
  // public hidden = true;
  /**
   * mostrar formulario para agregar direccion
   */
  public show = false;
  /**
   * ver parte trasera de la tarjeta
   */
  public showBack = false;
  /**
  * meses de vigencia para las tarjetas de crédito
  */
  public meses: any = [
    { mes: '01', name: '01 - Enero' },
    { mes: '02', name: '02 - Febrero' },
    { mes: '03', name: '03 - Marzo' },
    { mes: '04', name: '04 - Abril' },
    { mes: '05', name: '05 - Mayo' },
    { mes: '06', name: '06 - Junio' },
    { mes: '07', name: '07 - Julio' },
    { mes: '08', name: '08 - Agosto' },
    { mes: '09', name: '09 - Septiembre' },
    { mes: '10', name: '10 - Octubre' },
    { mes: '11', name: '11 - Noviembre' },
    { mes: '12', name: '12 - Diciembre' }
  ];

  /**
  * años vigencia de las tarjetas
  */
  public anios: any = [];

  /**
  * campo cvv
  */
  public cvv: string;

  /**
  * subscripción al servicio
  */
  public cvvSubscription: Subscription;

  /**
  * reglas de validacion de tarjeta de credito
  */
  public rules = {
    valid: true,
    nombre: { error: false },
    numero: { error: false },
    vencimiento: { error: false },
    codigo: { error: false },
    cliente: { error: false },
    telefono: { error: false }
  };

  /**
   * errores
  */
  public error: any;
  /**
  * bandera verificacion tarjeta departamental
  */
  public isDepartamental = false;

  public disabled = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cardValidatorService: CardValidatorService,
    private errorService: ErrorService,
    private globalService: GlobalService) { }

  ngOnInit() {

    if (this.route.snapshot.url[1].path === 'tarjeta-sears') {
      this.isDepartamental = true;
    }

    this.getYears();

    this.cvvSubscription = this.globalService.cvv
      .subscribe(cvv => {
        this.cvv = cvv;
      });

    this.errorService.errorActual.subscribe(error => {
      this.error = error;
    });
  }

  /**
  * método OnDestroy
  */
  ngOnDestroy() {
    this.errorService.setError('');
  }

  /**
   * verificacion del codigo cvv segun el tipo de tarjeta
   * @param {string} type
   * @returns {string}
   */
  public formatCode(type: string): string {
    let response: string;
    switch (type) {
      case 'amex':
        response = '****';
        break;
      default:
        response = '***';
        break;
    }
    return response;
  }

  /**
   * validacion del formulario de tarjeta
   * @param {string | undefined} field
   * @returns {boolean}
   */
  public validateForm(field: string | undefined): boolean {
    const response = this.cardValidatorService.validateCardForm(this.tarjetaForm);
    this.cvvHelp = response.tipo === 'amex' ? 'En caso de ser amex se muestra en la parte frontal de la tarjeta' : 'El cvv se encuentra en la parte posterior de la tarjeta';
    this.tarjetaForm.tipo = response.tipo;
    if (field !== undefined) {
      this.rules[field].error = response[field].error;
    } else {
      this.rules = response;
    }
    return response.valid;
  }

  /**
   * formato de tarjetas de credito
   * @param {string} digitos
   * @param {string} type
   * @returns {string}
   */
  public formatCard(digitos: string, type: string): string {
    let response = '';
    let gaps: number[] = [4, 8, 12];
    let card = '';

    switch (type) {
      case 'amex':
        card = digitos + '*********';
        gaps = [4, 10];
        break;
      case 'mastercard':
      case 'visa':
        card = digitos + '**********';
        gaps = [4, 8, 12];
        break;
      case 'sanborns':
      case 'sears':
        card = digitos + '******';
        gaps = [2, 8];
        break;
      default:
        card = digitos + '**********';
        gaps = [4, 8, 12];
        break;
    }
    for (let i = 0; i < card.length; i++) {
      for (const gap of gaps) {
        if (i === gap && i > 0) {
          response = response + ' ';
        }// end if
      }// end for
      response = response + card[i];
    }// end for

    return response;
  }

  /**
  * Define los parametros requeridos para el formulario tarjeta
  * @param {Tarjeta} tarjetaForm
  */
  setObjectTarjetaForm(tarjeta: Tarjeta): void {
    this.tarjetaForm.id = tarjeta.id;
    this.tarjetaForm.nombre = tarjeta.nombre + ' ' + tarjeta.apellido;
    this.tarjetaForm.numero = this.formatCard(tarjeta.bm, tarjeta.tipo);
    this.tarjetaForm.vencimiento = tarjeta.mes + '/' + tarjeta.anio;
    this.tarjetaForm.codigo = this.formatCode(tarjeta.tipo);
    this.tarjetaForm.tipo = tarjeta.tipo;
    this.tarjetaForm.telefono = tarjeta.telefono;
    this.tarjetaForm.cliente = tarjeta.cliente;
    this.expiry.mes = tarjeta.mes;
    this.expiry.anio = tarjeta.anio;
    this.expiry.expiry = tarjeta.mes + '/' + tarjeta.anio;
    if (tarjeta.tipo === 'sears') {
      this.expiry.expiry = '';
      this.isDepartamental = true;
    }
  }

  /**
  * Guardar el objeto tarjeta
  */
  save(): void {

    const validate = this.validateForm(undefined);
    if (validate) {
      this.tarjetaForm.numero = this.tarjetaForm.numero.replace(/[^0-9*]/g, '');
      // this.globalService.setCvv(this.tarjetaForm.codigo);
      // this.tarjetasService.addTarjeta(this.tarjetaForm)
      //   .subscribe((response: TarjetaResponse) => {
      //     this.setPago(response.id);
      //   });
      if (this.isDepartamental) {
        this.router.navigate(['/formas-pago/tarjeta-credito/promociones']);
      } else {
        this.router.navigate(['/formas-pago/tarjeta-credito/mensualidades']);
      }

    } else {

      if (this.isDepartamental) {
        this.error = 'Por favor verifica tu tarjeta departamental';
      }
    }// end if
  }

  /**
   * Actualiza el valor de la variable expiry
   */
  public updateExpiry() {
    const anio = this.expiry.anio === undefined ? '••' : this.expiry.anio;
    const mes = this.expiry.mes === undefined ? '••' : this.expiry.mes;
    this.expiry.expiry = mes + '/' + anio;
    this.tarjetaForm.vencimiento = this.expiry.expiry;
    if ((anio !== undefined && anio !== '••') && (mes !== undefined && mes !== '••')) {
      this.validateForm('vencimiento');
    }// end if
    setTimeout(function () {
      const event = document.createEvent('HTMLEvents');
      event.initEvent('keyup', false, true);
      const tarjeta: HTMLElement = document.querySelector('input[id="expiry"]') as HTMLElement;
      tarjeta.dispatchEvent(event);
    }, 50);
  }


  /**
  * establece el formato de las tarjetas
  */
  public initCardFormat(): void {
    setTimeout(function () {
      const event = document.createEvent('HTMLEvents');
      event.initEvent('keyup', false, true);
      const number: HTMLElement = document.querySelector('input[name="number"]') as HTMLElement;
      const name: HTMLElement = document.querySelector('input[name="name"]') as HTMLElement;
      const expiry: HTMLElement = document.querySelector('input[name="expiry"]') as HTMLElement;
      const code: HTMLElement = document.querySelector('input[name="cvc"]') as HTMLElement;
      number.dispatchEvent(event);
      name.dispatchEvent(event);
      expiry.dispatchEvent(event);
    }, 500);
  }

  /**
  * obtiene los años validos para el vencimiento de la tarjeta
  * @returns {any}
  */
  private getYears(): any {
    const year = new Date().getFullYear();
    for (let entry = year; entry < year + 8; entry++) {
      const text = entry.toString();
      const anio = text.substring(text.length - 2);
      this.anios.push({ anio: anio, name: text });
    }// end for
  }

  /**
  * regresa a la locacion lista de tarjetas guardadas
  */
  goListTarjetas(): void {
    this.router.navigate(['/formas-pago/guardadas']);
  }
}
