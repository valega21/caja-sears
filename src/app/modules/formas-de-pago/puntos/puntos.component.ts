import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-puntos',
  templateUrl: './puntos.component.html',
  styleUrls: ['./puntos.component.sass']
})
export class PuntosComponent implements OnInit {

  public title = 'Agregar Nueva Forma de Pago';
  public ruta = 'pagosActiva';
  public cvvHelp = 'El cvv se encuentra en la parte posterior de la tarjeta';
  public breadcrumb = true;
  public id = 0;

  constructor() { }

  ngOnInit() {
  }

}
