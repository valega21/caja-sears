import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../../../core/services/global.service';
import { DepositoService } from '../../../core/services/deposito.service';

@Component({
  selector: 'app-deposito-transferencia',
  templateUrl: './deposito-transferencia.component.html',
  styleUrls: ['./deposito-transferencia.component.sass']
})
export class DepositoTransferenciaComponent implements OnInit {

  public title = 'Selecciona un Banco';
  public ruta = 'pagosActiva';
  public breadcrumb = true;
  public id = 0;
  private banco = 'bancomer';
  private datos: any = { banco: this.banco };

  public default = 'bancomer';

  public bancos: any = [
    { nombre: 'Pago desde el extranjero', img: '../assets/logo-mastercard.svg' },
    { nombre: 'Bancomer', img: '../assets/logo-bancomer.svg' },
    { nombre: 'Santander', img: '../assets/logo-santander.svg' },
    { nombre: 'Banamex', img: '../assets/logo-banamex.svg' },
  ];

  constructor(
    private router: Router,
    private globalService: GlobalService,
    private depositoService: DepositoService
  ) { }

  ngOnInit() { }

  /**
   * Define el banco
   * @param $event
   * @param value
   */
  public selectBanco($event, value) {

    $event.preventDefault();
    this.banco = value;
    this.datos = { banco: this.banco };
    this.setBanco(this.datos).then((banco) => {
      this.router.navigate(['/confirmacion']);
    });
  }

  /**
   * Genera el request al servicio banco
   * @param datos
   */
  public setBanco(datos) {
    return this.depositoService.setBanco(datos).toPromise();
  }

}
