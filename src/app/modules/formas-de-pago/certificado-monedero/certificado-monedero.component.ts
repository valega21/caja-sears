import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
/** modelos */
import { Validaciones } from '../../../core/modelos/validaciones';
/** servicios */
import { ValidatorService } from '../../../core/services/validator.service';
import { ErrorService } from '../../../core/services/error.service';
import { MonederoService } from '../../../core/services/monedero.service';
import { Monedero } from '../../../core/modelos/monedero';

@Component({
  selector: 'app-certificado-monedero',
  templateUrl: './certificado-monedero.component.html',
  styleUrls: ['./certificado-monedero.component.sass']
})
export class CertificadoMonederoComponent implements OnInit {

  public monedero: Monedero = new Monedero();
  public title = 'Agregar Nueva Forma de Pago';
  public subtitle = 'Selecciona una forma de pago';
  public ruta = 'pagosActiva';
  public breadcrumb = true;
  public id = 0;
  public error: string;
  public loading = false;
  public hide = true;
  // public monedero: any;

  public credit = 0;
  public total = 0;
  public amount = 0;

  public rules = {
    valid: true,
    numero: { error: false },
    tipo: { error: false }
  };

  constructor(
    private validatorService: ValidatorService,
    private router: Router,
    private monederoService: MonederoService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

  public validate(): void {
    this.goConfirmacion();
  }

  public validateForm(field: string | undefined): boolean {
    const response = this.monederoService.validateFormMonedero(this.monedero);

    if (field !== undefined) {
      this.rules[field].error = response[field].error;
    } else {
      this.rules = response;
    }
    return response.valid;
  }

  public goConfirmacion(): void {
    const validate = this.validateForm(undefined);
    if (validate) {
      this.router.navigate(['/confirmacion']);
    }
  }

  public goFormasPago(): void {
    this.router.navigate(['/formas-pago']);
  }


}
