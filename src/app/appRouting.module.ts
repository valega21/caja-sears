import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DireccionesComponent } from './modules/direcciones/direcciones.component';
import { DireccionNuevaComponent } from './modules/direcciones/direccion-nueva/direccion-nueva.component';
import { DireccionDetalleComponent } from './modules/direcciones/direccion-detalle/direccion-detalle.component';
import { FormasDePagoComponent } from './modules/formas-de-pago/formas-de-pago.component';
import { TarjetaCreditoComponent } from './modules/formas-de-pago/tarjeta-credito/tarjeta-credito.component';
import { CertificadoMonederoComponent } from './modules/formas-de-pago/certificado-monedero/certificado-monedero.component';
import { DepositoTransferenciaComponent } from './modules/formas-de-pago/deposito-transferencia/deposito-transferencia.component';
import { PuntosComponent } from './modules/formas-de-pago/puntos/puntos.component';
import { ThankyouPageComponent } from './modules/thankyou-page/thankyou-page.component';
import { ConfirmacionComponent } from './modules/confirmacion/confirmacion.component';
import { MensualidadesComponent } from './modules/formas-de-pago/tarjeta-credito/mensualidades.component/mensualidades.component';
import { PromocionesComponent } from './modules/formas-de-pago/tarjeta-credito/promociones/promociones.component';

const routes: Routes = [

    // Raiz
    { path: '', redirectTo: 'direcciones', pathMatch: 'full' },

    // Modulo Direcciones
    { path: 'direcciones', component: DireccionesComponent },
    { path: 'direcciones/agregar', component: DireccionNuevaComponent },
    { path: 'direcciones/detalle/:id', component: DireccionDetalleComponent },

    // Modulo Formas de Pago
    { path: 'formas-pago', component: FormasDePagoComponent },
    { path: 'formas-pago/tarjeta-sears', component: TarjetaCreditoComponent },
    { path: 'formas-pago/tarjeta-sears/:id', component: TarjetaCreditoComponent },
    { path: 'formas-pago/tarjeta-credito/promociones', component: PromocionesComponent },

    { path: 'formas-pago/tarjetas-credito', component: TarjetaCreditoComponent },
    { path: 'formas-pago/tarjetas-credito/:id', component: TarjetaCreditoComponent },
    { path: 'formas-pago/tarjeta-credito/mensualidades', component: MensualidadesComponent },

    { path: 'confirmacion', component: ConfirmacionComponent },
    { path: 'formas-pago/certificado-monedero', component: CertificadoMonederoComponent },
    { path: 'formas-pago/puntos-sears', component: PuntosComponent },
    { path: 'formas-pago/deposito-transferencia', component: DepositoTransferenciaComponent },

    // Modulo ThankyouPage
    { path: 'gracias-por-tu-compra', component: ThankyouPageComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
