/**
 * modelo para la fecha de vencimiento de las tarjetas
 */
export class Expiry {
    /**
     * mes de vencimiento
     */
    mes: string;
    /**
     * año de vencimiento
     */
    anio: string;
    /**
     * fecha de expiración
     */
    expiry: string;
}
