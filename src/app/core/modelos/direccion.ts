/**
 * Modelo Dirección
 */
export class Direccion {
    /**
     * identificador
     */
    id: number;
    /**
     * nombre de quien recibe
     */
    nombre: string;
    /**
     * apellido paterno de quien recibe
     */
    apellidoP: string;
    /**
     * apellido materno de quien recibe
    */
    apellidoM: string;
    /**
     * teléfono
     */
    telefono: string;
    /**
     * persona autorizada para recibir
    */
    personaAutorizada: string;
    /**
     * estado
     */
    estado: string;
    /**
     * ciudad
     */
    ciudad: string;
    /**
     * colonias
     */
    colonia: string;
    /**
     * calle
     */
    calle: string;
    /**
     * código postal
     */
    codigoPostal: string;
    /**
     * número exterior
     */
    numeroExterior: string;
    /**
     * número interior
     */
    numeroInterior: string;
    /**
     * entre calles
     */
    entreCalles: string;
    /**
    * entre calle
    */
    yCalle: string;
    /**
     * referencias
     */
    referencias: string;
    /**
     * municipio
     */
    municipio: string;
    /**
     * edificacion: casa, edificio, oficina
     */
    edificacion: string;
    /**
     * nivel de acceso: 1er piso, 2do, etc
     */
    nivel: string;
    /**
     * forma de acceso: escaleras, elevador, volado
     */
    forma: string;
    /**
     * código de estado
     */
    codigoEstado: string;
}
