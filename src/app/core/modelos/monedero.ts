/**
 * Modelo Monedero
 */
export class Monedero {
    /**
     * numero de monedero o certificado
     */
    numero: string;
    /**
     * tipo monedero o certificado
     */
    tipo: number;
    /**
     * estado
     */
    status: boolean;
    /**
     * bandera procesar
     */
    procesa: boolean;
    /**
     * saldo disponible
     */
    saldo: number;
    /**
     * monto
     */
    monto: number;
    /**
     * total
     */
    total: number;
}
