import { Direccion } from './direccion';

/**
 * modelo Multipedido
 */
export class Multipedido {
    /**
     * identificador de cliente
     */
    idCliente: number;
    /**
     * nombre del pedido
     */
    nombre: string;
    /**
     * identificador forma de pago
     */
    idFormaDePago: number;
    /**
     * identificador direccion
     */
    idDireccion: number;
    /**
     * forma de pago
     */
    formaDePago: any;
    /**
     * número de mensualidades
     */
    mensualidades: number;
    /**
     * total
     */
    total: number;
    /**
     * envio
     */
    envio: number;
    /**
     * intentos
     */
    attempts: number;
    /**
     * bandera si es click y recoge
     */
    clickRecoge: boolean;
    /**
     * sucursal
     */
    sucursal: number;
    /**
     * nombre de la promoción
     */
    nombrePromocion: string;
    /**
     * facturación
     */
    facturacion: Direccion;
    /**
     * monto de pago
     */
    montoPago: number;
}
