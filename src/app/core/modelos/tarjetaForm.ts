/**
 * modeo TarjetaForm
 */
export class TarjetaForm {
    /**
     * identificador
     */
    id: number | string;
    /**
     * campo para el nombre del titular
     */
    nombre: string;
    /**
     * campo para el apellido del titular
     */
    apellido: string;
    /**
     * numero
     */
    numero: string;
    /**
     * vencimiento de la tarjeta
     */
    vencimiento: string;
    /**
     * codigo
     */
    codigo: string;
    /**
     * tipo
     */
    tipo: string;

    /**
     * telefono del titular
     */
    telefono: string;

    /**
     * numero de cliente internet
     */
    cliente: string;
}
