/**
 * Modelo Checkout, utilizado para la verificación
 */
export class Checkout {
    /**
     * código de verificación
     */
    codigo: number;
    /**
     * deviceFinger
     */
    deviceFinger: number;
    /**
     * estado de verificación
     */
    status: boolean;
    /**
     * número de error
     */
    error: number;
    /**
     * mensaje de error
     */
    message: string;
    /**
     * action
     */
    action: string;
    /**
     * data
     */
    data: any;
    /**
     * número de intentos
     */
    attempts: number;
    /**
     * token
     */
    token: string;
    /**
     * identificador payer
     */
    payerID: string;
    /**
     * dirección url
     */
    url: string;
    /**
     * transacción
     */
    transaccion: string;
    /**
     * número de cupón
     */
    cupon: string;
    /**
     * monto del carrito
     */
    importe: number;
}
