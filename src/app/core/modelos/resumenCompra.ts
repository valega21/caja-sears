/**
 * modelo ResumenCompra
 */
export class ResumenCompra {
    /**
     * costo de envio
     */
    costoEnvio: number;
    /**
     * descuento pago tarjeta credito
     */
    descuentoCredito: number;
    /**
     * descuento de cupon
     */
    descuentoCupon: number;
    /**
     * descuento forma de pago
     */
    descuentoFpago: number;
    /**
     * grupos
     */
    grupos: any;
    /**
     * productos de la compra
     */
    productos: any;
    /**
     * sub total
     */
    subtotal: number;
    /**
     * total a pagar
     */
    total: number;
  }
