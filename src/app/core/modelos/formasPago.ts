/**
 * modelo formaPago
 */
export class FormaPago {
    /**
     * identificador menu forma de pago
     */
    idMenuFormasdePago: number;
    /**
     * forma de pago
     */
    idFormadePago: number;
    /**
     * nombre forma de pago
     */
    formadePago: string;
    /**
     * logo forma de pago
     */
    logo: string;
    /**
     * descripción de la forma de pago
     */
    descripcion: string;
    /**
     * tipo
     */
    tipo: number;
    /**
     * estado
     */
    status: number;
    /**
     * orden
     */
    order: number;
}
