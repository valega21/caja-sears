/**
 * Modelo para la estructura de Cupón
 */
export class Cupon {
    /**
     * bandera estado del cupon
     */
    status: boolean;
    /**
     * número de error
     */
    error: number;
    /**
     * mensaje de error
     */
    message: string;
    /**
     * número del cupón
     */
    cupon: string;
    /**
     * valor del cupón
     */
    importe: number;
}
