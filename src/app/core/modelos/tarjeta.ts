/**
 * modelo Tarjeta
 */
export class Tarjeta {
    /**
     * id
     */
    id: number;
    /**
     * nombre
     */
    nombre: string;
    /**
     * digitos
     */
    digitos: string;
    /**
     * apellido
     */
    apellido: string;
    /**
     * mes
     */
    mes: string;
    /**
     * año
     */
    anio: string;
    /**
     * bandera
     */
    flag: boolean;
    /**
     * tipo
     */
    tipo: string;
    /**
     * predet
     */
    predet: string;
    /**
     * bm
     */
    bm: string;
    /**
     * id forma de pago
     */
    idFormaPago: number;
    /**
     * token
     */
    token: string;

    telefono: string;

    cliente: string;
}
