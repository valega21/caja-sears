/**
 * modelo para la zonificación del codigo postal
 */
export class Zonificacion {
    /**
     * estado
     */
    estado: string;
    /**
     * municipio
     */
    municipio: string;
    /**
     * ciudad
     */
    ciudad: string;
    /**
     * listado de colonias
     */
    colonias: string[];
    /**
     * bandera estado
     */
    estatus: boolean;
    /**
     * error
     */
    error: string;
    /**
     * codigo de estado
     */
    codigoEstado: string;
}
