/**
 * modelo de los campos para las validaciones
 */
export class Validaciones {

    /**
     * name
     */
    public name: string;

    /**
     * value
     */
    public value: string;

    /**
     * message
     */
    public message: string;

    /**
     * error
     */
    public error: boolean;

    /**
     * type
     */
    public type: string;

    /**
     * data
     */
    public data: any;

    /**
     * constructor, inicializa:
     * @param {string} name
     * @param {string} value
     * @param {string} type
     * @param {string} message
     * @param {boolean} error
     * @param {any} data
     */

    constructor(name: string = '', value: string = '', type: string = 'string', message: string = '', error: boolean = false, data: any = {}) {
        this.name = name;
        this.value = value;
        this.type = type;
        this.message = message;
        this.error = error;
        this.data = data;
    }
}
