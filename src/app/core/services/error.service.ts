import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

/**
 * servicio para establecer los errores ocurridos.
 */
@Injectable()

export class ErrorService {
  /**
   * obtiene el valor actual del errorSource
   */
  private errorSource = new BehaviorSubject('');

  /**
   * data errorSource
   */
  errorActual = this.errorSource.asObservable();

  /**
   * constructor
   */
  constructor() { }

  /**
   * establece el error
   * @param {any} error
   */
  setError(error: any) {
    this.errorSource.next(error);
  }

}
