import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Zonificacion } from '../modelos/zonificacion';
import { catchError } from 'rxjs/operators';

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()
export class ZonificacionService {


  private zonificacion: Zonificacion[] = [
    {
      estado: 'Distrito Federal',
      municipio: 'Miguel Hidalgo',
      ciudad: 'Ciudad de México',
      colonias: ['Anáhuac I Sección'],
      estatus: true,
      error: '',
      codigoEstado: '11320'
    },
    {
      estado: 'Distrito Federal',
      municipio: 'Miguel Hidalgo',
      ciudad: 'Ciudad de México',
      colonias: ['Anzures'],
      estatus: true,
      error: '',
      codigoEstado: '11590'
    },
    {
      estado: 'Distrito Federal',
      municipio: 'Cuauhtémoc',
      ciudad: 'Ciudad de México',
      colonias: ['Cuauhtémoc'],
      estatus: true,
      error: '',
      codigoEstado: '06500'
    },
    {
      estado: 'Distrito Federal',
      municipio: 'Cuauhtémoc',
      ciudad: 'Ciudad de México',
      colonias: ['Condesa'],
      estatus: true,
      error: '',
      codigoEstado: '06140'
    },
    {
      estado: 'Distrito Federal',
      municipio: 'Cuauhtémoc',
      ciudad: 'Ciudad de México',
      colonias: ['San Simón Tolnáhuac'],
      estatus: true,
      error: '',
      codigoEstado: '06920'
    }
  ];

  // constructor(private http: HttpClient) {
  //   console.log('Servicio Listo para Usarse.');
  // }

  constructor(private http: HttpClient) {
    this.getZonificacion().subscribe(data => {
        console.log(data);
    });
}

public getZonificacion(): Observable<any> {
  return this.http.get(`https://api.myjson.com/bins/zktpa`);
}
  // getZonificacion(cp: string) {
    // getZonificacion() {
    //   return this.zonificacion;
    // }


  }
