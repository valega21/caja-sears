import { Injectable } from '@angular/core';
import { Validaciones } from '../modelos/validaciones';

/**
 * servicio encargado de las validaciones de los campos,
 * email, int, string, alphanumeric, codigo postal,
 * cvv, numero de telefono, captcha
 */
@Injectable()

export class ValidatorService {

  /**
   * array validator
   */
  public Validators: Validaciones[];

  /**
   * constructor
   */
  constructor() { }

  /**
   * Valida el metodo
   * @param data
   */
  public validate(data) {
    this.process(data);
    return data;
  }

  /**
   * proceso de los parametros para la data
   * @param params
   */
  private process(params) {
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        switch (params[key].type) {
          case 'string':
            params[key].error = this.isValidString(params[key].value) ? false : true;
            break;
          case 'int':
            params[key].error = this.isValidInt(params[key].value) ? false : true;
            break;
          case 'email':
            params[key].error = this.isValidEmail(params[key].value) ? false : true;
            break;
          case 'number':
            params[key].error = this.isValidNumber(params[key].value) ? false : true;
            break;
          case 'postalCode':
            params[key].error = this.isValidPostalCode(params[key].value) ? false : true;
            break;
          case 'phone':
            params[key].error = this.isValidPhone(params[key].value) ? false : true;
            break;
          case 'alphanumeric':
            params[key].error = this.isValidAlphaNumeric(params[key].value) ? false : true;
            break;
          case 'formatstring':
            params[key].error = this.isValidFormatString(params[key].value) ? false : true;
            break;
          case 'cvv':
            params[key].error = this.isValidCvv(params[key].value) ? false : true;
            break;
        }// end switch
      }// end if
    }

    return params;
  }

  /**
   * Valida que el valor sea un string
   * @param {string} value
   * @returns {boolean}
   */
  private isValidString(value: string): boolean {

    let response = false;
    if (value !== undefined) {
      response = /(^[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+)$/.test(value);
    }// end if

    return response;
  }

  /**
   * Valida que el valor sea alfanumerico
   * @param {string} value
   * @returns {boolean}
   */
  private isValidAlphaNumeric(value: string): boolean {

    let response = false;
    if (value !== undefined) {
      response = /(^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ\s]+)$/.test(value);
    }// end if

    return response;
  }

  /**
   * Valida que el valor sea un string
   * @param {string} value
   * @return {boolean}
   */
  private isValidFormatString(value: string): boolean {

    let response = false;
    if (value !== undefined) {
      response = /(^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ()_@./%?#&+°,"-\s]+)$/.test(value);
    }// end if

    return response;
  }
  /**
   * Valida que el campo sea un int
   * @param {string} value
   * @return {boolean}
   */
  private isValidInt(value: string): boolean {

    let response = false;
    if (value !== undefined) {
      response = /(^[1-9]+)$/.test(value);
    }// end if

    return response;
  }

  /**
   * Valida la dirección de email
   * @param {string} value
   * @returns {boolean}
   */
  private isValidEmail(value: string): boolean {

    let response = false;
    if (value !== undefined) {
      response = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
    }// end if

    return response;
  }

  /**
   * Valida el numeero
   * @param {string} value
   * @return {boolean}
   */
  private isValidNumber(value: string): boolean {

    let response = false;
    if (value !== undefined) {
      response = /(^[0-9]+)$/.test(value);
    }// end if

    return response;
  }

  /**
   * Valida el código postal
   * @param {string} value
   * @return {boolean}
   */
  private isValidPostalCode(value: string): boolean {

    let response = false;
    if (value !== undefined) {
      response = /(^[0-9]{5})$/.test(value);
    }// end if

    return response;
  }

  /**
   * Valida el número de télefono
   * @param {string} value
   * @return {boolean}
   */
  private isValidPhone(value: string): boolean {

    let response = false;
    if (value !== undefined) {
      response = /(^[0-9\s]{10})$/.test(value.trim());
    }// end if

    return response;
  }

  /**
   * Valida el código cvv de tarjetas de crédito
   * @param {string} value
   * @return {boolean}
   */
  private isValidCvv(value: string): boolean {

    let response = false;
    if (value !== undefined) {
      response = /(^[0-9]{3,4})$/.test(value);
    }// end if

    return response;
  }
}
