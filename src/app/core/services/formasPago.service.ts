import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';
import { FormaPago } from '../modelos/formasPago';

@Injectable()
export class FormasPagoService {

    private fp: FormaPago[] = [
        {
            idMenuFormasdePago: 1,
            idFormadePago: 1,
            formadePago: 'Tarjeta departamental Sears',
            logo: './assets/logo-sears.svg',
            descripcion: 'tarjeta departamental sears',
            tipo: 1,
            status: 1,
            order: 1
        },
        {
            idMenuFormasdePago: 2,
            idFormadePago: 2,
            formadePago: 'Tarjeta Visa / Mastercard / AMEX',
            logo: './assets/logo-sears.svg',
            descripcion: 'tarjetas de crédito',
            tipo: 2,
            status: 2,
            order: 2
        },
        {
            idMenuFormasdePago: 3,
            idFormadePago: 3,
            formadePago: 'Paypal',
            logo: './assets/logo-paypal.svg',
            descripcion: 'Paypal',
            tipo: 3,
            status: 3,
            order: 3
        },
        {
            idMenuFormasdePago: 4,
            idFormadePago: 4,
            formadePago: 'Pago en Tienda Sears',
            logo: './assets/logo-sears.svg',
            descripcion: 'Pago en Tienda Sears',
            tipo: 4,
            status: 4,
            order: 4
        },
        {
            idMenuFormasdePago: 5,
            idFormadePago: 5,
            formadePago: 'Certificado / Monedero Sears',
            logo: './assets/logo-certificado-monedero-sears.jpg',
            descripcion: 'Certificado / Monedero Sears',
            tipo: 5,
            status: 5,
            order: 5
        },
        {
            idMenuFormasdePago: 6,
            idFormadePago: 6,
            formadePago: 'Puntos Sears',
            logo: './assets/logo-puntos-sears.webp',
            descripcion: 'Puntos Sears',
            tipo: 6,
            status: 6,
            order: 6
        },
        {
            idMenuFormasdePago: 7,
            idFormadePago: 7,
            formadePago: 'Depósito y Transferencia Bancaria',
            logo: './assets/logo-bancos.svg',
            descripcion: 'Depósito y Transferencia Bancaria',
            tipo: 7,
            status: 7,
            order: 7
        }
    ];
    constructor(private http: HttpClient) {
    }

    /**
    * Obtiene las formas de pago registradas
    * @returns {FormaPago[]}
    */
    // getFormasDePago(): Observable<FormaPago[]> {
    //     return this.http.get<FormaPago[]>(this.endPoint, httpGetOptions)
    //         .pipe(
    //             catchError(this.handleError('getFormasDePago', []))
    //         );
    // }

    public getFormasDePago() {
        return this.fp;
    }
}
