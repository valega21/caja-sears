import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { Checkout } from '../modelos/checkout';
import { Monedero } from '../modelos/monedero';
import { Config } from '../modelos/config';
import { ConfigService } from './config.service';

/**
 * encabezados del servicio
 */
const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })

};

/**
 * encabezados del servicio
 */
const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()
export class MonederoService {
  private config: Config = new Config();

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  public validateFormMonedero(tarjeta: Monedero) {
    const response = {
      valid: false,
      tipo: { error: false },
      numero: { error: false }
    };
    const numero = tarjeta.numero ? tarjeta.numero.replace(/[^0-9*]/g, '') : '';
    const tipo = tarjeta.tipo ? tarjeta.tipo : '';

    const validNumero = (numero.length === 6) ? true : false;
    const validTipo = (tipo > 0) ? true : false;

    response.numero.error = validNumero ? false : true;
    response.tipo.error = validTipo ? false : true;

    response.valid = (validNumero && validTipo) ? true : false;

    return response;
  }





}
