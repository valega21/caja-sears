import { Injectable } from '@angular/core';
import { TarjetaForm } from '../modelos/tarjetaForm';

/**
 * tarjeta visa
 */
const VISA = 'visa';
/**
 * tarjeta mastercard
 */
const MASTERCARD = 'mastercard';
/**
 * tarjeta american express
 */
const AMERICAN_EXPRESS = 'amex';
/**
 * tarjeta sears
 */
const SEARS = 'sears';
/**
 * tarjeta sanborns
 */
const SANBORNS = 'sanborns';
/**
 * codigo cvv
 */
const CVV = 'CVV';
/**
 * codigo cid
 */
const CID = 'CID';
/**
 * codigo cvc
 */
const CVC = 'CVC';
/**
 * valor por defecto de numero de años validos
 */
const DEFAULT_VALID_NUMBER_OF_YEARS_IN_THE_FUTURE = 20;
/**
 * array tarjetas
 */
const ORIGINAL_TEST_ORDER = [SEARS, SANBORNS, VISA, MASTERCARD, AMERICAN_EXPRESS];

/**
 * servicio encargado de la validación de los campos de tarjeta de crédito,
 * codigo cvv, número de tarjeta de crédito, fecha de vencimiento.
 */
@Injectable()

export class CardValidatorService {

  /**
   * constructor
   */
  constructor() { }

  /** funcion de verificación isNan */
  isNaN: Function = Number.isNaN;

  /**
   * objeto de tipo de tarjetas
   */
  private testOrder = ORIGINAL_TEST_ORDER;

  /**
   * tipos de tarjeta de credito
   */
  private types = {
    visa: {
      niceType: 'Visa',
      type: VISA,
      prefixPattern: /^4$/,
      exactPattern: /^4\d*$/,
      gaps: [4, 8, 12],
      lengths: [16, 18, 19],
      code: {
        name: CVV,
        size: 3
      }
    },
    mastercard: {
      niceType: 'Mastercard',
      type: MASTERCARD,
      prefixPattern: /^(5|5[1-5]|2|22|222|222[1-9]|2[3-6]|27|27[0-2]|2720)$/,
      exactPattern: /^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[0-1]|2720)\d*$/,
      gaps: [4, 8, 12],
      lengths: [16],
      code: {
        name: CVC,
        size: 3
      }
    },
    amex: {
      niceType: 'American Express',
      type: AMERICAN_EXPRESS,
      prefixPattern: /^(3|34|37)$/,
      exactPattern: /^3[47]\d*$/,
      isAmex: true,
      gaps: [4, 10],
      lengths: [15],
      code: {
        name: CID,
        size: 4
      }
    },
    sanborns: {
      niceType: 'Sanborns',
      type: SANBORNS,
      prefixPattern: /^11|14|29|34|42|47|72|83|93|97/,
      exactPattern: /^(11|14|29|34|42|47|72|83|93|97){1}[0-9]{10}$/,
      gaps: [2, 7],
      lengths: [12],
      code: {
        name: CID,
        size: 3
      }
    },
    sears: {
      niceType: 'Sears',
      type: SEARS,
      prefixPattern: /^0[1-3]|06|09|10|16|1[2-3]|1[8-9]|24|26|27|3[0-1]|35|37|39|43|44|46|49|5[0-1]|56|60|6[2-3]|68|70|73|74|79|8[0-1]|85|87|90|92|94|96|98|2[1-3]/,
      exactPattern: /^(0[1-3]|06|09|10|16|1[2-3]|1[8-9]|24|26|27|3[0-1]|35|37|39|43|44|46|49|5[0-1]|56|60|6[2-3]|68|70|73|74|79|8[0-1]|85|87|90|92|94|96|98|2[1-3]){1}[0-9]{10}$/,
      gaps: [2, 7],
      lengths: [12],
      code: {
        name: CID,
        size: 3
      }
    },
  };

  /**
   * Ejecuta validacion del objeto formulario
   * @param {TarjetaForm} tarjeta
   */
  public validateCardForm(tarjeta: TarjetaForm) {
    const response = {
      valid: false,
      tipo: '',
      nombre: { error: false },
      numero: { error: false },
      vencimiento: { error: false },
      codigo: { error: false },
      cliente: { error: false },
      telefono: { error: false },
      apellido: { error: false }
    };
    let card = tarjeta.numero ? tarjeta.numero.replace(/[^0-9*]/g, '') : '';
    const name = tarjeta.nombre ? tarjeta.nombre.trim() : '';
    const apellido = tarjeta.apellido ? tarjeta.apellido.trim() : '';
    const telefono = tarjeta.telefono ? tarjeta.telefono.trim() : '';
    const cliente = tarjeta.cliente ? tarjeta.cliente.trim() : '';
    let code = tarjeta.codigo ? tarjeta.codigo.trim() : '';
    let idCard = tarjeta.id ? tarjeta.id : '';
    const date = tarjeta.vencimiento ? tarjeta.vencimiento : '';
    let type = tarjeta.tipo;
    if (type === 'sears' || type === 'sanborns') {
      card = card.replace(/[^0-9]/g, '0');
    } else {
      card = card.replace(/[^0-9]/g, '');
    }

    let codeSize = 3;
    const validCard = this.cardNumber(card);
    const validDate = this.expirationDate(date, DEFAULT_VALID_NUMBER_OF_YEARS_IN_THE_FUTURE);

    if (validCard.card == null) {
      codeSize = 0;
      type = '';
    } else {
      if (validCard.isValid === true && (validCard.card.type === 'sears' || validCard.card.type === 'sanborns')) {
        codeSize = 3;
        idCard = 123;
        code = '123';
        validDate.isValid = true;
      } else {
        codeSize = validCard.card.code.size ? validCard.card.code.size : 3;
      }
      type = validCard.card.type;
    }
    let validCode = (codeSize > 0 && codeSize === code.length) ? true : false;
    const validName = (name.length > 2) ? true : false;

    const validApellido = (apellido.length > 2) ? true : false;
    const validCliente = (cliente.length > 2) ? true : false;
    const validTelefono = (telefono.length === 10) ? true : false;

    if (idCard > 0 || this.verifyUUIDv4(idCard)) {
      validCard.isValid = true;
      codeSize = (type === 'amex') ? 4 : 3;
      validCode = (codeSize > 0 && codeSize === code.length) ? true : false;
    }
    response.tipo = type;
    response.nombre.error = validName ? false : true;

    response.apellido.error = validApellido ? false : true;

    response.cliente.error = validCliente ? false : true;
    response.telefono.error = validTelefono ? false : true;

    response.numero.error = validCard.isValid ? false : true;
    response.codigo.error = validCode ? false : true;
    response.vencimiento.error = validDate.isValid ? false : true;
    response.valid = (validCard.isValid && validDate.isValid && validName && validCode) ? true : false;

    return response;
  }

  /**
   * verificar identificador unico universal de la tarjeta
   * @param idCard
   */
  private verifyUUIDv4(idCard) {
    const regex = new RegExp('^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$');
    return regex.test(idCard);
  }

  /**
   * verificación de tarjeta
   * @param card
   * @param isPotentiallyValid
   * @param isValid
   */
  private verification(card, isPotentiallyValid, isValid) {
    return {
      card: card,
      isPotentiallyValid: isPotentiallyValid,
      isValid: isValid
    };
  }

  /**
   * verificación número de tarjeta segun el tipo
   * @param value
   */
  private cardNumber(value) {
    let cardType, i, isPotentiallyValid, isValid, maxLength, potentialTypes;
    potentialTypes = void 0;
    cardType = void 0;
    isPotentiallyValid = void 0;
    isValid = void 0;
    i = void 0;
    maxLength = void 0;
    if (typeof value === 'number') {
      value = String(value);
    }
    if (typeof value !== 'string') {
      return this.verification(null, false, false);
    }
    value = value.replace(/\-|\s/g, '');
    if (!/^\d*$/.test(value)) {
      return this.verification(null, false, false);
    }
    potentialTypes = this.creditCardType(value);
    if (potentialTypes.length === 0) {
      return this.verification(null, false, false);
    } /*else if (potentialTypes.length !== 1) {
      return this.verification(null, true, false);
    }*/
    cardType = potentialTypes[0];
    if (cardType.type === SEARS || cardType.type === SANBORNS) {
      // Sears y Sanborns are not Luhn 10 compliant
      isValid = true;
    } else {
      isValid = this.luhn10(value);
    }
    maxLength = Math.max.apply(null, cardType.lengths);

    i = 0;
    while (i < cardType.lengths.length) {
      if (cardType.lengths[i] === value.length) {
        isPotentiallyValid = value.length !== maxLength || isValid;
        return this.verification(cardType, isPotentiallyValid, isValid);
      }
      i++;
    }
    return this.verification(cardType, value.length < maxLength, false);
  }

  /**
   * suma de verificación base 10
   * @param identifier
   */
  private luhn10(identifier) {
    let alt, i, num, sum;
    sum = 0;
    alt = false;
    i = identifier.length - 1;
    num = void 0;
    while (i >= 0) {
      num = parseInt(identifier.charAt(i), 10);
      if (alt) {
        num *= 2;
        if (num > 9) {
          num = num % 10 + 1;
          // eslint-disable-line no-extra-parens
        }
      }
      alt = !alt;
      sum += num;
      i--;
    }
    return sum % 10 === 0;
  }

  /**
   * clonar
   * @param originalObject
   */
  private clone(originalObject) {
    let dupe;
    dupe = void 0;
    if (!originalObject) {
      return null;
    }
    dupe = JSON.parse(JSON.stringify(originalObject));
    delete dupe.prefixPattern;
    delete dupe.exactPattern;
    return dupe;
  }

  /**
   * encontrar tipo
   * @param type
   */
  private findType(type) {
    return this.types[type];
  }

  /**
   * tipo de tarjeta de credito
   * @param cardNumber
   */
  private creditCardType(cardNumber) {
    let exactResults, i, prefixResults, type, value;
    type = void 0;
    value = void 0;
    i = void 0;
    prefixResults = [];
    exactResults = [];
    if (!(typeof cardNumber === 'string' || cardNumber instanceof String)) {
      return [];
    }
    i = 0;
    while (i < this.testOrder.length) {
      type = this.testOrder[i];
      value = this.findType(type);
      if (cardNumber.length === 0) {
        prefixResults.push(this.clone(value));
        i++;
        continue;
      }
      if (value.exactPattern.test(cardNumber)) {
        exactResults.push(this.clone(value));
      } else if (value.prefixPattern.test(cardNumber)) {
        prefixResults.push(this.clone(value));
      }
      i++;
    }
    if (exactResults.length) {
      return exactResults;
    } else {
      return prefixResults;
    }
  }

  /**
   * verificacion de que la fecha sea un campo valido
   * se ingreso el año y el mes
   * @param isValid
   * @param isPotentiallyValid
   * @param month
   * @param year
   */
  private verificationDate(isValid, isPotentiallyValid, month, year) {
    return {
      isValid: isValid,
      isPotentiallyValid: isPotentiallyValid,
      month: month,
      year: year
    };
  }

  /**
   * comprobación de la fecha completa de vencimiento de la tarjeta
   * el año y el mes son validos
   * @param value
   * @param maxElapsedYear
   */
  private expirationDate(value, maxElapsedYear) {
    let date, isValidForThisYear, monthValid, yearValid;
    date = void 0;
    monthValid = void 0;
    yearValid = void 0;
    isValidForThisYear = void 0;
    if (typeof value === 'string') {
      value = value.replace(/^(\d\d) (\d\d(\d\d)?)$/, '$1/$2');
      date = this.parseDate(value);
    } else if (value !== null && typeof value === 'object') {
      date = {
        month: String(value.month),
        year: String(value.year)
      };
    } else {
      return this.verificationDate(false, false, null, null);
    }
    monthValid = this.expirationMonth(date.month);
    yearValid = this.expirationYear(date.year, maxElapsedYear);
    if (monthValid.isValid) {
      if (yearValid.isCurrentYear) {
        isValidForThisYear = monthValid.isValidForThisYear;
        return this.verificationDate(isValidForThisYear, isValidForThisYear, date.month, date.year);
      }
      if (yearValid.isValid) {
        return this.verificationDate(true, true, date.month, date.year);
      }
    }
    if (monthValid.isPotentiallyValid && yearValid.isPotentiallyValid) {
      return this.verificationDate(false, true, null, null);
    }
    return this.verificationDate(false, false, null, null);
  }

  /**
   * verification mounth
   * @param isValid
   * @param isPotentiallyValid
   * @param isValidForThisYear
   */
  private verificationMonth(isValid, isPotentiallyValid, isValidForThisYear) {
    return {
      isValid: isValid,
      isPotentiallyValid: isPotentiallyValid,
      isValidForThisYear: isValidForThisYear || false
    };
  }

  /**
   * mes de vencimiento de la tarjeta
   * @param {string} value
   */
  private expirationMonth(value: string) {
    let currentMonth, month, result;
    month = void 0;
    result = void 0;
    currentMonth = (new Date).getMonth() + 1;
    if (typeof value !== 'string') {
      return this.verificationMonth(false, false, false);
    }
    if (value.replace(/\s/g, '') === '' || value === '0') {
      return this.verificationMonth(false, true, false);
    }
    if (!/^\d*$/.test(value)) {
      return this.verificationMonth(false, false, false);
    }
    month = parseInt(value, 10);
    if (this.isNaN(value)) {
      return this.verificationMonth(false, false, false);
    }
    result = month > 0 && month < 13;
    return this.verificationMonth(result, result, result && month >= currentMonth);
  }

  /**
   * verifica que el año sea un valor valido
   * @param isValid
   * @param isPotentiallyValid
   * @param isCurrentYear
   */
  private verificationYear(isValid, isPotentiallyValid, isCurrentYear) {
    return {
      isValid: isValid,
      isPotentiallyValid: isPotentiallyValid,
      isCurrentYear: isCurrentYear || false
    };
  }

  /**
   * verificación del año de vencimiento de las tarjetas
   * @param value
   * @param maxElapsedYear
   */
  private expirationYear(value, maxElapsedYear) {
    let currentFirstTwo, currentYear, firstTwo, isCurrentYear, len, twoDigitYear, valid;
    currentFirstTwo = void 0;
    currentYear = void 0;
    firstTwo = void 0;
    len = void 0;
    twoDigitYear = void 0;
    valid = void 0;
    isCurrentYear = void 0;
    maxElapsedYear = maxElapsedYear || DEFAULT_VALID_NUMBER_OF_YEARS_IN_THE_FUTURE;
    if (typeof value !== 'string') {
      return this.verificationYear(false, false, false);
    }
    if (value.replace(/\s/g, '') === '') {
      return this.verificationYear(false, true, false);
    }
    if (!/^\d*$/.test(value)) {
      return this.verificationYear(false, false, false);
    }
    len = value.length;
    if (len < 2) {
      return this.verificationYear(false, true, false);
    }
    currentYear = (new Date).getFullYear();
    if (len === 3) {
      // 20x === 20x
      firstTwo = value.slice(0, 2);
      currentFirstTwo = String(currentYear).slice(0, 2);
      return this.verificationYear(false, firstTwo === currentFirstTwo, false);
    }
    if (len > 4) {
      return this.verificationYear(false, false, false);
    }
    value = parseInt(value, 10);
    twoDigitYear = Number(String(currentYear).substr(2, 2));
    if (len === 2) {
      isCurrentYear = twoDigitYear === value;
      valid = value >= twoDigitYear && value <= twoDigitYear + maxElapsedYear;
    } else if (len === 4) {
      isCurrentYear = currentYear === value;
      valid = value >= currentYear && value <= currentYear + maxElapsedYear;
    }
    return this.verificationYear(valid, valid, isCurrentYear);
  }

  /**
   * transforma la fecha
   * @param value
   */
  private parseDate(value) {
    let len, month, year, yearValid;
    month = void 0;
    len = void 0;
    year = void 0;
    yearValid = void 0;
    if (/\//.test(value)) {
      value = value.split(/\s*\/\s*/g);
    } else if (/\s/.test(value)) {
      value = value.split(RegExp(' +', 'g'));
    }
    if (this.isArray(value)) {
      return {
        month: value[0],
        year: value.slice(1).join()
      };
    }
    len = value[0] === '0' || value.length > 5 ? 2 : 1;
    if (value[0] === '1') {
      year = value.substr(1);
      yearValid = this.expirationYear(year, DEFAULT_VALID_NUMBER_OF_YEARS_IN_THE_FUTURE);
      if (!yearValid.isPotentiallyValid) {
        len = 2;
      }
    }
    month = value.substr(0, len);
    return {
      month: month,
      year: value.substr(month.length)
    };
  }

  /**
   * verifica si es un array
   * @param arg
   */
  private isArray(arg) {
    return Object.prototype.toString.call(arg) === '[object Array]';
  }

}
