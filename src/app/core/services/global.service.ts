import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class GlobalService {

  private cvvSource = new BehaviorSubject('');
  /**
 * data cvv
 */
  public cvv = this.cvvSource.asObservable();

  constructor() { }

}
