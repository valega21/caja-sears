import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';

const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()
export class DepositoService {

  private endPoint: '/formas-pago/';

  constructor(private http: HttpClient) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  setBanco(banco: any): Observable<any> {
    const url = this.endPoint + '/deposito-transferencia';
    const data = JSON.stringify(banco);
    return this.http.post<any>(url, data, httpSetOptions).pipe(
      catchError(this.handleError<any>('setBanco', []))
    );
  }

  getBanco(): Observable<any> {
    const url = this.endPoint + '/deposito-transferencia';
    return this.http.get<any>(url, httpGetOptions)
      .pipe(
        catchError(this.handleError<any>('getBanco'))
      );
  }
}
